package elearning.com.moo_b.activities;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.persistence.RealmHelper;
import elearning.com.moo_b.rest.RestHelper;

public class DetailActivity extends AppCompatActivity implements RestHelper.RestDelegate, View.OnClickListener {

    private static final String TAG = "DetailActivity";


    // especificamos de forma pública cómo pasar datos a esta activity

    public static final String KEY_INTENT_TITLE = "detailactivity.title";

    public static final String KEY_INTENT_IMG = "detailactivity.img_url";

    public static final String KEY_INTENT_ID = "detailactivity.id";





    //blabla, monton de vistas.
    private ImageView poster;
    private CollapsingToolbarLayout toolbarLayout;
    private RestHelper restHelper;
    private RealmHelper realmHelper;
    private TextView genre;
    private TextView director;
    private TextView actors;
    private TextView plot;
    private TextView titleView;
    private ImageView bigPoster;
    private View bigPosterContainer;
    private FloatingActionButton fab;
    private Drawable favorite;
    private Drawable noFavorite;

    private Animation pulse;


    //aqui tenemos los datos conocidos al arrancar la activity
    private String imageUrl;
    private String title;
    private String id;

    //aqui, tendremos la pelicula si ya era favorita o cuando nos llegue del server
    private Movie mMovie;

    //flag para almacenar el estado temporal fav/nofav
    boolean isFav;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Aqui obtenemos una animacion. Se carga desde un xml (pulse.xml)
        // e indica a la vista como tiene que animarse (cambio de limites, escalado, fade, la duracion, etc).
        // Es un tema muy complejo y con muchisimas opciones de personalización, aquí solo veremos como usar una animación
        // para añadir un efecto más agradable al efecto de pulsar un boton. Ver como se lanza la animación en el onClick del FloatingActionButton
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);


        // Recuperamos del intent los datos necesarios para funcionar.
        // Se debería comprobar que los datos vienen correctamente.
        Intent intent = getIntent();
        imageUrl = intent.getStringExtra(KEY_INTENT_IMG);
        title = intent.getStringExtra(KEY_INTENT_TITLE);
        id = intent.getStringExtra(KEY_INTENT_ID);

        restHelper = new RestHelper(this);
        realmHelper = new RealmHelper(this);

        findViews();
        load();


    }

    @Override
    protected void onStop() {
        super.onStop();

        //La activity va a cerrarse: persistimos el estado final de favorito.
        //el usuario puede haberle dado mil veces al boton, pero solo tendremos en cuenta como lo ha dejado
        //cuando la activity se cierra
        if (isFav) {
            realmHelper.saveFavorite(mMovie);
        } else {
            realmHelper.removeFromFavorite(mMovie);
        }
        realmHelper.close();
    }


    private void load() {

        toolbarLayout.setTitle(title);
        titleView.setText(title);
        Picasso.with(this)
                .load(imageUrl)
                .error(R.drawable.no_img)
                .into(poster, new Callback() {

                    //cuando acabemos, queremos hacer cosillas con las imagenes:
                    @Override
                    public void onSuccess() {
                        setPalette();
                    }


                    @Override
                    public void onError() {
                        setPalette();
                    }
                });

        bigPoster.setOnClickListener(this);
        poster.setOnClickListener(this);

        //si la pelicula esta en base de datos, significa que es favorita (y que tenemos sus datos, asi que no hace falta descargar nada, usaremos la que tenemos)
        mMovie = realmHelper.getMovieById(id);

        if (mMovie == null) {
            restHelper.searchById(id, this);
        } else {
            bind(mMovie);
            isFav = true;
            setupFab();

        }


        Picasso.with(this)
                .load(imageUrl)
                .error(R.drawable.no_img)
                .into(bigPoster);

    }


    /**
     * Este metodo extrae una paleta de colores del bitmap del poster. Lo usamos para dar color a la toolbar,
     * que de este modo dependera de los colores del poster. ver la docu oficial para mas info
     */
    private void setPalette() {


        //se hace con un callback porque puede tardar bastante en generarla.
        Palette.from(((BitmapDrawable) poster.getDrawable()).getBitmap()).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                toolbarLayout.setBackgroundColor(palette.getVibrantColor(toolbarLayout.getSolidColor()));
                toolbarLayout.setContentScrimColor(palette.getMutedColor(toolbarLayout.getSolidColor()));
            }
        });


    }

    private void findViews() {

        bigPoster = (ImageView) findViewById(R.id.big_poster);
        bigPosterContainer = findViewById(R.id.big_poster_container);
        poster = (ImageView) findViewById(R.id.toolbar_poster);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        genre = (TextView) findViewById(R.id.genre);
        director = (TextView) findViewById(R.id.director);
        actors = (TextView) findViewById(R.id.actors);
        plot = (TextView) findViewById(R.id.plot);
        titleView = (TextView) findViewById(R.id.film_title);
        fab = (FloatingActionButton) findViewById(R.id.fab);


    }

    @Override
    public void onMovieSearchResult(List<Movie> movies) {
        // nada
    }

    @Override
    public void onMovieSearchFailure() {
        // nada
    }

    @Override
    public void onMovieDetailResult(Movie movie) {

        mMovie = movie;

        bind(mMovie);

        isFav = false; // si la hemos pedido a servidor, es que no la tenemos -> no es favorita

        setupFab();
    }

    private void setupFab() {

        // nota: fab -> FloatingActionButton

        fab.setVisibility(View.VISIBLE);

        //alternaremos entre el corazon lleno y vacio para indicar si es favorito o no.
        //podemos usar cualquier sistema, siempre y cuando el usuario lo entienda sin explicarselo.
        favorite = ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_white_24dp).mutate();
        noFavorite = ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_border_white_24dp);


        // *para más iconos: "https://material.io/icons/". Proporciona iconos basicos para incluir en la app (menú sobretodo)


        syncFavoriteDrawable();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //cuando el usuario haga click, alternaremos el estado
                isFav = !isFav;
                //cambiaremos el icono
                syncFavoriteDrawable();

                //y mostraremos una animación que deja claro que hemos tenido en cuenta la accion.
                //el boton aplicara las transformaciones indicadas a sus valores
                fab.startAnimation(pulse);

                // solo guardaremos si actualmente es favorito o no.
                // Traspasaremos al final de todo ese valor a la base de datos, añadiendo o eliminando el objeto.
                // Así evitamos eliminar-insertar-eliminar-insertar en cada click sobre el boton. Ver onStop()
            }
        });


    }

    private void syncFavoriteDrawable() {

        //haremos que el fab refleje el estado real
        fab.setImageDrawable(isFav ? favorite : noFavorite);
    }


    private void bind(Movie movie) {

        genre.setText(movie.getGenre());
        actors.setText(movie.getActors());
        director.setText(String.format("%s, %s", movie.getDirector(), movie.getYear()));
        plot.setText(movie.getPlot());

    }


    @Override
    public void onBackPressed() {

        //Este metodo esta disponible para override en todas las activities.
        //indica que el usuario ha pulsado back.
        //podemos evitar el cierre NO LLAMANDO a super.onBackPressed().


        //en este caso, si estamos mostrando la version ampliada del poster, cerramos el poster,
        //y si no dejamos salir al usuario


        //nunca hay que impedir que el usuario abandone una activity sin un buen motivo, el dispositivo
        // es suyo y no debemos decidir cuando puede o no salir de nuestra app.

        //en este caso lo interceptamos porque, de ser el poster visible, lo mas probable es que el usuario
        //quiera que se quite de enmedio. En el resto de casos cerramos la activity (dejando que super.onBackPressed() haga su trabajo)

        if (bigPosterContainer.getVisibility() == View.VISIBLE) {
            bigPosterContainer.setVisibility(View.INVISIBLE);
        } else {

            super.onBackPressed();
        }

    }

    @Override
    public void onMovieDetailFailure() {
        // nada
    }

    @Override
    public void onClick(View v) {

        //si el usuario toca la foto grande o la barra de tareas, mostramos u ocultamos la imagen grande
        //ver el metodo load(), donde seteamos el mismo onClickListener para ambas vistas.
        if (bigPosterContainer.getVisibility() != View.VISIBLE) {

            bigPosterContainer.setVisibility(View.VISIBLE);
        } else {
            bigPosterContainer.setVisibility(View.INVISIBLE);
        }
    }
}
