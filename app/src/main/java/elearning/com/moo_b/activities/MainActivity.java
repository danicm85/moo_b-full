package elearning.com.moo_b.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.adapters.MovieAdapter;
import elearning.com.moo_b.favs.FavsActivity;
import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.persistence.RealmHelper;
import elearning.com.moo_b.providers.SuggestionProvider;
import elearning.com.moo_b.rest.RestHelper;

public class MainActivity extends AppCompatActivity implements RestHelper.RestDelegate, MovieAdapter.ListCallback {


    private static final String TAG = "MainActivity";

    private RestHelper restHelper;
    private TextView banner;
    private RecyclerView list;
    private MovieAdapter adapter;
    private SearchView searchView;
    private MenuItem searchItemMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        restHelper = new RestHelper(this);

        findViews();


    }

    @Override
    protected void onNewIntent(Intent intent) {

        // Este método se llama cada vez que nuestra activity recibe un nuevo intent.

        // Normalmente, solo recibimos uno, el que arranca la activity. En este caso
        // recibimos las búsquedas como un intent, por lo que necesitamos saber cada vez que llega uno nuevo.

        // ver la docu: https://developer.android.com/guide/topics/search/search-dialog.html


        // no es necesario utilizar estas modificaciones respecto al proyecto, pero es interesante ver una manera
        // de integrar la búsqueda de una forma más 'android'.
        // Podeis mantener el campo de busqueda y el searchIcon con su onclick, y la app funciona igual.


        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            //Recuperamos el texto solicitado por el usuario (Titulo de la pelicula a buscar)
            String query = intent.getStringExtra(SearchManager.QUERY);

            // Creamos una instancia de la clase que gestiona las sugerencias de busqueda (para mostrarlas al usuario)
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);

            //Guardamos la busqueda actual en las sugerencias, android se encarga de persistirlas y mostrarlas
            suggestions.saveRecentQuery(query, null);

            //Aqui hacemos lo normal, buscar las peliculas
            searchByTitle(query);
        }
    }

    private void findViews() {


        list = (RecyclerView) findViewById(R.id.movie_list);
        banner = (TextView) findViewById(R.id.no_resultados_banner); // cuando la busqueda no devuelva resultados, ocultaremos la lista y mostraremos un texto
        banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  searchView.onActionViewExpanded();
                searchView.setIconified(false);
            }
        });


        list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MovieAdapter(this);
        list.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        searchItemMenu = menu.findItem(R.id.search);
        //Lo que viene a continuacion esta copypasteado de la docu de google sobre la SearchView.
        //Usaremos lo que nos dicen a rajatabla, pero es un overkill ( vereis que le pasamos las busquedas por intent a nuestra propia activity).

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.my_favs) {
            startActivity(new Intent(this, FavsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void searchByTitle(String title) {
        if (title.isEmpty()) {
            Toast.makeText(this, R.string.busqueda_vacia, Toast.LENGTH_SHORT).show();
        } else {
            restHelper.searchByTitle(title, this);
        }
    }


    private void setDataView(boolean hasData) {

        // Si determinamos que hay datos, nos aseguramos que la lista sea visible
        // y el banner de aviso desaparezca. Lo contrario si no hay datos.

        if (hasData) {
            banner.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        } else {
            banner.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }
    }


    private void hideSoftInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(list.getWindowToken(), 0);
    }


    @Override
    public void onMovieSearchResult(List<Movie> movies) {





        //Comprobamos si hay algo que mostrar, seteamos los
        //datos en consecuencia y actualizamos la vista

        boolean hasData = movies != null && !movies.isEmpty();


        if(hasData){
           // searchView.clearFocus();
           // searchView.onActionViewCollapsed();
            MenuItemCompat.collapseActionView(searchItemMenu);
         //   invalidateOptionsMenu();
        }


        adapter.setData(hasData ? movies : null);

        setDataView(hasData);


    }

    @Override
    public void onMovieSearchFailure() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMovieDetailResult(Movie movie) {
    }

    @Override
    public void onMovieDetailFailure() {
    }

    @Override
    public void itemClicked(Movie movie) {


        // Preparamos un intent cuyo destino será una DetailActivity
        Intent intent = new Intent(this, DetailActivity.class);

        //Añadimos los datos que vamos a necesitar
        intent.putExtra(DetailActivity.KEY_INTENT_TITLE, movie.getTitle());
        intent.putExtra(DetailActivity.KEY_INTENT_IMG, movie.getPosterUrl());
        intent.putExtra(DetailActivity.KEY_INTENT_ID, movie.getImdbID());

        //Solicitamos al sistema que arranque la activity con el intent que hemos configurado
        startActivity(intent);
    }

}
