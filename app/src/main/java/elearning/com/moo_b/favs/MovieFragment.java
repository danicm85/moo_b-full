package elearning.com.moo_b.favs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;

/**
 * Created by danielcruz on 05/05/2017.
 * <p>
 * Copyright © 2017 SIGMA AIE. All rights reserved.
 */

public class MovieFragment extends Fragment {


    private static final String ARGS_KEY_MOVIE_ID = "args_key.movie_id";
    private static final String ARGS_KEY_MOVIE_TITLE = "args_key.movie_title";
    private static final String ARGS_KEY_MOVIE_GENRE = "args_key.movie_genre";
    private static final String ARGS_KEY_MOVIE_DIRECTOR = "args_key.movie_director";
    private static final String ARGS_KEY_MOVIE_ACTORS = "args_key.movie_actors";
    private static final String ARGS_KEY_MOVIE_PLOT = "args_key.movie_plot";
    private static final String ARGS_KEY_MOVIE_POSTER = "args_key.movie_poster";


    private ImageView poster;
    private TextView genre;
    private TextView director;
    private TextView actors;
    private TextView plot;
    private TextView titleView;

    private String mId;
    private String mTitle;
    private String mGenre;
    private String mDirector;
    private String mActors;
    private String mPlot;
    private String mPosterUrl;


    //metodo factoría para generar fragments que reflejen una movie.
    //usar siempre algo como esto en lugar de un constructor (google dice).
    public static Fragment getInstance(Movie movie) {

        //por si se nos va la olla, no tiene sentido sin una peli valida
        if (movie == null || movie.getImdbID().trim().isEmpty()) {
            throw new IllegalArgumentException("provide a valid movie");
        }

        //Creamos un fragment, y un bundle para sus argumentos.
        Fragment instance = new MovieFragment();
        Bundle arguments = new Bundle();

        //metemos la peli en el bundle
        //no lo metemos como serializable porque es incompatible con realm.

        arguments.putString(ARGS_KEY_MOVIE_ID, movie.getImdbID());
        arguments.putString(ARGS_KEY_MOVIE_TITLE, movie.getTitle());
        arguments.putString(ARGS_KEY_MOVIE_GENRE, movie.getGenre());
        arguments.putString(ARGS_KEY_MOVIE_DIRECTOR, movie.getDirector());
        arguments.putString(ARGS_KEY_MOVIE_ACTORS, movie.getActors());
        arguments.putString(ARGS_KEY_MOVIE_PLOT, movie.getPlot());
        arguments.putString(ARGS_KEY_MOVIE_POSTER, movie.getPosterUrl());


        //le seteamos los args y lo retornamos.
        instance.setArguments(arguments);

        return instance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //recuperamos los argumentos y nos los guardamos para cuando haya vista que setear

        Bundle args = getArguments();
        mId = args.getString(ARGS_KEY_MOVIE_ID);
        mTitle = args.getString(ARGS_KEY_MOVIE_TITLE);
        mGenre = args.getString(ARGS_KEY_MOVIE_GENRE);
        mDirector = args.getString(ARGS_KEY_MOVIE_DIRECTOR);
        mActors = args.getString(ARGS_KEY_MOVIE_ACTORS);
        mPlot = args.getString(ARGS_KEY_MOVIE_PLOT);
        mPosterUrl = args.getString(ARGS_KEY_MOVIE_POSTER);

    }

    //si no sobreescribimos este metodo no se crea la vista
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // a partir de aqui ya tendremos vista, antes de retornarla la acabaremos de setear.
        View mView = inflater.inflate(R.layout.movie_fragment, container, false);
        findViews(mView);
        bind();
        return mView;
    }


    private void findViews(View mView) {

        poster = (ImageView) mView.findViewById(R.id.poster);
        genre = (TextView) mView.findViewById(R.id.genre);
        director = (TextView) mView.findViewById(R.id.director);
        actors = (TextView) mView.findViewById(R.id.actors);
        plot = (TextView) mView.findViewById(R.id.plot);
        titleView = (TextView) mView.findViewById(R.id.film_title);

    }


    private void bind() {

        Picasso.with(getContext())
                .load(mPosterUrl)
                .resizeDimen(R.dimen.fav_pic_width, R.dimen.fav_pic_height)
                .error(R.drawable.no_img)
                .into(poster);


        genre.setText(mGenre);
        director.setText(mDirector);
        actors.setText(mActors);
        plot.setText(mPlot);
        titleView.setText(mTitle);

    }


}
