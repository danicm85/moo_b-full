package elearning.com.moo_b.favs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import elearning.com.moo_b.model.Movie;

/**
 *
 *
 * Clase que proporciona fragments con datos de los favoritos al viewpager
 *
 *
 * Created by danielcruz on 05/05/2017.
 * <p>
 * Copyright © 2017 SIGMA AIE. All rights reserved.
 */

 class MoviePagerAdapter extends FragmentStatePagerAdapter {


    // contrato para inyectar los datos en el adapter, sin tener que
    // gestionarlos desde aqui.
    interface DataDelegate {

        int getTotal();
        Movie getItemAtPosition(int position);
    }

    private DataDelegate mDelegate;

     MoviePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    void setDelegate(DataDelegate delegate) {
        this.mDelegate = delegate;
    }



    @Override
    public Fragment getItem(int position) {

        // lo requiere el adapter: creamos UN NUEVO FRAGMENT cada vez que lo pida.
        // mirando por internet vereis hacks que se guardan los fragments en una lista etc.. bad,
        // a veces el adapter se destruye pero los fragments se reutilizan, por lo que en el nuevo adapter
        // no tendreis esas referencias.



        checkState();

        //dejamos que el delegate decida que hay en esa posicion.
        return MovieFragment.getInstance(mDelegate.getItemAtPosition(position));
    }


    // este normalmente no se sobreescribe, pero
    // se lia bastante si eliminamos fragments mientras se estan mostrando.
    @Override
    public int getItemPosition(Object object){
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        //lo requiere el adapter: el número total de paginas a mostrar
        return mDelegate != null ? mDelegate.getTotal() : 0;
    }


    private void checkState() {
        if (mDelegate == null) {
            throw new IllegalStateException("no DataDelegate specified");
        }
    }




}
