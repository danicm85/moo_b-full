package elearning.com.moo_b.rest;

import android.content.Context;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.model.MovieSearch;
import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by danielcruz
 */

public class RestHelper {


    private static final String TAG = "RestHelper";

    private final RestServices services;
    private final Retrofit client;


    public RestHelper(Context context) {


        // Este código se modifica respecto a lo que hemos visto
        // en clase, es solo una forma de decirle al serializador Gson que, ante la duda,
        // ignore todo lo que pertenezca a RealmObject. Evitaremos problemas en algunas operaciones.

        Gson gson =  new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();



        // obtenemos del archivo xml llamado config.xml la url del servidor. Es buena costumbre extraer todos
        // los parametros configurables en un archivo de resources: Podeis, sin cambiar mucho mas, hacer
        // varias versiones de la misma app solo tuneando las configuraciones (una app para leer de otro server, por ejemplo)
        String baseUrl = context.getString(R.string.base_url);

        // montamos el retrofit client, que nos permite acceder a nuestros servicios
        client = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        //pedimos al cliente retrofit que saque una instancia de nuestra interfaz para poder usarla
        services = client.create(RestServices.class);

    }


    public void searchByTitle(String title, final RestDelegate delegate) {

        // encolamos una llamada en segundo plano. Cuando termine, bien o mal, llamara a onResponse o onFailure.
        // entonces pasaremos al delegate (objeto encargado de procesar y entender la respuesta) los datos recibidos.
        services.moviesByTitle(title).enqueue(new Callback<MovieSearch>() {
            @Override
            public void onResponse(Call<MovieSearch> call, Response<MovieSearch> response) {

                delegate.onMovieSearchResult(response.body().getResults());
            }

            @Override
            public void onFailure(Call<MovieSearch> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                delegate.onMovieSearchFailure();
            }
        });
    }


    public void searchById(String id, final RestDelegate delegate) {

        services.movieByImdb(id).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                delegate.onMovieDetailResult(response.body());
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                delegate.onMovieDetailFailure();
            }
        });


    }


    public interface RestDelegate {

        void onMovieSearchResult(List<Movie> movies);

        void onMovieSearchFailure();

        void onMovieDetailResult(Movie movie);

        void onMovieDetailFailure();

    }


}
