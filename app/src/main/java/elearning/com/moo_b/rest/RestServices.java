package elearning.com.moo_b.rest;

import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.model.MovieSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by danielcruz
 */

interface RestServices {


    // Aqui se configuran las diferentes llamadas al servidor, transformandolas en metodos java.
    // La libreria los convertira en metodos plenamente funcionales.

    @GET("?r=json&type=movie&plot=short")
    Call<MovieSearch> moviesByTitle(@Query("s") String title);


    @GET("?r=json&type=movie&plot=full")
    Call<Movie> movieByImdb(@Query("i") String IMDb);

}
