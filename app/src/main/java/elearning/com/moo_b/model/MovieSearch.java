package elearning.com.moo_b.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Clase que representa una búsqueda en servidor.
 * Created by danielcruz
 */
public class MovieSearch {




    @SerializedName("Search")       // en el json del servidor, results se llama Search. Se lo indicamos con el @SerializedName al parseador.
    private List<Movie> results;


    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
