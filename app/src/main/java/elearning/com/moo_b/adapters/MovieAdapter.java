package elearning.com.moo_b.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;

/**
 * Created by danielcruz
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> implements View.OnClickListener {


    private final ListCallback callback;
    private List<Movie> data;


    // Vereis que el adapter no funciona igual que el del curso. En esta version, vamos
    // a obligar a que se pase en el constructor para asegurarnos que existe.
    // Ademas, usamos una cosilla nueva para indicar que no queremos nulos: @NonNull.
    // Si android studio detecta que pasais un callback nulo, o que puede serlo, os pondra una advertencia avisando de ello.
    public MovieAdapter(@NonNull ListCallback callback) {

        this.callback = callback;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Primero, inflamos el layout (inflar => crear una instancia y configurarla a través de un xml)
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_movie_linear, parent, false);

        //seteamos el adapter (this se refiere al objeto que invoca) como onClickListener
        view.setOnClickListener(this);

        //creamos y retornamos el viewholder para que el adapter pueda utilizarlo
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // mediante la position determinamos el objeto que corresponde pintar de nuestro conjunto de datos

        Movie movie = data.get(position);

        //aqui, durante la formación añadimos el holder como tag. Os dejo esta versión
        //para que veais otra forma de hacerlo. El onClick por lo tanto también cambia
        holder.itemView.setTag(position);


        // actualizamos el contendido del holder con los datos de la pelicula en cuestión
        holder.title.setText(movie.getTitle());
        holder.year.setText(movie.getYear());

        // pedimos a la librería que intente cargar el poster de la pelicula dentro de nuestro holder (holder.poster)
        Picasso.with(holder.itemView.getContext())
                .load(movie.getPosterUrl())
                .error(R.drawable.no_img)
                .into(holder.poster);


    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<Movie> getData() {
        return data;
    }

    public void setData(List<Movie> data) {


        // cada vez que hacemos un cambio en nuestro conjunto de datos, avisamos al adapter
        // de que probablemente los datos que esta mostrando se le hayan quedado viejunos.
        this.data = data;
        notifyDataSetChanged();



        // EXTRA //

        // llamar a notifyDataSetChanged() indica que TODO el conjunto de datos se ha invalidado.
        // si lo que hacemos es añadir un objeto en una posicion concreta, o eliminarlo, podemos
        // usar métodos más concretos. Esto permite a la lista animar las vistas (por ejemplo, si añadimos un item
        // en el centro de la lista, veremos como anima los que estan por debajo como si los empujase).

        // notifyItemChanged(position) -> indicar que repinte un solo objeto de la lista. Deja intacto el resto y ademas añade una animacion tipo fade para que el usuario se de cuenta de que ha cambiado
        // notifyItemInserted(position) -> indicar que hemos añadido en tal position un item. Empujara a los siguientes
        // notifyItemRemoved(position) -> indicar que hemos eliminado un elemento de la lista. Lo hara desaparecer y empujara a los de debajo hacia arriba.
        // +++ hay mas versiones para añadir o eliminar rangos, etc

        // tened en cuenta que indicar que eliminamos un elemento tiene que ir acompañado de eliminar DE VERDAD el elemento. Por ejemplo:
        // data.remove(position);
        // si no, tendreis inconsistencias entre lo que el usuario ve y los datos reales.

        // todo esto podeis usarlo para, por ejemplo, eliminar favoritos directamente desde su lista.
    }

    @Override
    public void onClick(View v) {

        //recuperamos el numero que representa la posicion del tag de la view
        Movie item = data.get((Integer) v.getTag());

        callback.itemClicked(item);


    }

    // contrato que tiene que cumplir un objeto para ser notificado de los clicks sobre peliculas
    public interface ListCallback {
        void itemClicked(Movie movie);
    }


    // clase que retiene referencias de una row de la lista,
    // para poder usarla una y otra vez minimizando las búsquedas en arbol de vistas a 1 por row.
    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final ImageView poster;
        final TextView year;

        ViewHolder(View itemView) {
            super(itemView);

            poster = (ImageView) itemView.findViewById(R.id.row_movie_poster);
            title = (TextView) itemView.findViewById(R.id.row_movie_title);
            year = (TextView) itemView.findViewById(R.id.row_movie_year);

        }
    }
}
